module Mutations
  class CreatePost < BaseMutation
    # TODO: define return fields
    field :post, Types::PostType, null: false

    # TODO: define arguments
    argument :title, String, required: true
    argument :body, String, required: true

    # TODO: define resolve method
    def resolve(title:, body:)
      user = User.first
      post = user.posts.new(title: title, body: body)

      if post.save
        {
          post: post,
          errors: [], 
        }
      else
        {
          post: nil,
          errors: post.errors.full_messages,
        }
      end
    end
  end
end
