module Mutations
  class DestroyPost < BaseMutation
    # TODO: define return fields
    field :post, Types::PostType, null: false

    # TODO: define arguments
    argument :id, Integer, required: true
    # argument :title, String, required: true
    # argument :body, String, required: true

    # TODO: define resolve method
    def resolve(id:)
      post = Post.find(id)
      post.destroy
      {
        id: id
      }
    end
  end
end
