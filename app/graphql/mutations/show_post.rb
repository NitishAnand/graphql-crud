module Mutations
  class ShowPost < BaseMutation
    # TODO: define return fields
    field :post, Types::PostType, null: false

    # TODO: define arguments
    argument :id, Integer, required: true

    # TODO: define resolve method
    def resolve(id:)
      begin
        post = Post.find(id)
        {
          post:  post,
          errors: []
        }
      rescue => e 
        GraphQL::ExecutionError.new('Post does not exist.')
      end
    end
  end
end
