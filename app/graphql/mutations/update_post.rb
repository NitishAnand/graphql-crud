module Mutations
  class UpdatePost < BaseMutation
    # TODO: define return fields
    field :post, Types::PostType, null: false

    # TODO: define arguments
    argument :id, Integer, required: true
    argument :title, String, required: true
    argument :body, String, required: true
    # TODO: define resolve method
    def resolve(id:, title:, body:)
      post = Post.find(id)
      if post.update(title: title, body: body)
        { 
          post: post,
          errors: []
        }
      else
        {
          post: nil,
          errors: post.errors.full_messages
        }
      end
    end
  end
end
